package com.spring.ftf.springfeatureflag.controller;

import com.spring.ftf.springfeatureflag.domain.CustomerInfo;
import com.spring.ftf.springfeatureflag.service.FeatureFlagService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class FeatureFlagController {

    private final FeatureFlagService featureFlagService;

    @GetMapping("/info")
    public Collection<CustomerInfo> getCustomerInfos(@RequestParam(required = false) String uname,
                                                     @RequestParam(required = false) String sessionId) {

        return featureFlagService.getCustomers(uname, sessionId);
    }

}
