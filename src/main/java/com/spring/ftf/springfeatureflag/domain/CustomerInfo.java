package com.spring.ftf.springfeatureflag.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerInfo {
    private String uname;
    private String fullname;
}
