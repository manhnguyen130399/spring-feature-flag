package com.spring.ftf.springfeatureflag.service;

import com.spring.ftf.springfeatureflag.config.UnleashConfiguration;
import com.spring.ftf.springfeatureflag.domain.CustomerInfo;
import io.getunleash.UnleashContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class FeatureFlagService {
    private final UnleashConfiguration unleashConfiguration;
    public Collection<CustomerInfo> getCustomers(String uname, String sessionId) {

        final List<CustomerInfo> infos = new ArrayList<>();
        infos.add(new CustomerInfo("B", "Nguyen B"));
        if(unleashConfiguration.unleash().isEnabled("feature-flag-prod",
                new UnleashContext(uname, sessionId,"", new HashMap<>() ))) {
            infos.add(new CustomerInfo("A", "Nguyen A"));
        }
        return infos;
    }
}
