package com.spring.ftf.springfeatureflag.config;

import io.getunleash.DefaultUnleash;
import io.getunleash.Unleash;
import io.getunleash.util.UnleashConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UnleashConfiguration {
    @Bean
    public Unleash unleash() {
        return new DefaultUnleash(config());
    }

    private UnleashConfig config() {
        return
                UnleashConfig.builder()
                        .appName("production")
                        .instanceId("VgMmN9njv2DMxMSriKSH")
                        .unleashAPI("https://gitlab.com/api/v4/feature_flags/unleash/45100593")
                        .apiKey("glpat-2zFyc1ARkMSyUoCuRhzf")
                        .build();
    }
}
