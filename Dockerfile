FROM maven:3.6.0-jdk-11-slim AS build
WORKDIR /app
COPY . .

RUN mvn clean install && mvn test

FROM openjdk:11-jre-slim
COPY --from=build /app/target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]

